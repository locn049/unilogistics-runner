package com.uni.unilogisticsrunner.base

sealed class DataState<out R> {
    data class Success<out T>(
        val data: T,
        val nextPage: String? = null,
        val prevPage: String? = null,
        val pageSize: Int? = null,
        val pageIndex: Int? = null,
        val totalPages: Int? = null
    ) : DataState<T>()

    data class ServerFailed(val code: String?, val message: String?) : DataState<Nothing>()
    data class Error(val exception: Throwable) : DataState<Nothing>()
    object Loading : DataState<Nothing>()
    object InitNothing : DataState<Nothing>()
    object NetworkError: DataState<Nothing>()
}