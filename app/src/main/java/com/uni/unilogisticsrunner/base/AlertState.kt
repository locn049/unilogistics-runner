package com.uni.unilogisticsrunner.base

import com.uni.unilogisticsrunner.constants.ServerErrors
import com.uni.unilogisticsrunner.utils.extensions.nullToEmpty

sealed class AlertState {
    data class Error(var code: String?, var message: String?) : AlertState()
    data class Warning(var code: String?, var message: String?) : AlertState()
    data class Existed(var code: String?, var message: String?) : AlertState()
    data class Success(var code: String?, var message: String?) : AlertState()
    object Unknown : AlertState() {
        var code: String? = null
        var message: String? = null
    }
}

fun toAlertState(code: String?, message: String?): AlertState {
    return when (code) {
        ServerErrors.SUCCESS ->
            AlertState.Success(code.nullToEmpty(), message.nullToEmpty())
        ServerErrors.ERROR ->
            AlertState.Error(code.nullToEmpty(), message.nullToEmpty())
        ServerErrors.WARNING ->
            AlertState.Warning(code.nullToEmpty(), message.nullToEmpty())
        ServerErrors.EXISTED ->
            AlertState.Existed(code.nullToEmpty(), message.nullToEmpty())
        else -> AlertState.Unknown
    }
}