package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class AthleteActivityModel(
    val badge_type_id: Int? = null,
    val bio: String? = null,
    val city: String? = null,
    val country: String? = null,
    val created_at: String? = null,
    val firstname: String? = null,
//    val follower: ,
//    val friend: Any,
    val id: Int? = null,
    val lastname: String? = null,
    val premium: Boolean? =null,
    val profile: String? = null,
    val profile_medium: String? =null,
    val resource_state: Int? = null,
    val sex: String? = null,
    val state: String? = null,
    val summit: Boolean? = null,
    val updated_at: String? = null,
    val username: String? = null,
    val weight: Double? = null
)