package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class InfoActivity
    (
    var today: ActivityPer? = null,
    var weekly: ActivityPer? = null
)
