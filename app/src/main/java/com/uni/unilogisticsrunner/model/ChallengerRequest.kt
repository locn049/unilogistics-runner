package com.uni.unilogisticsrunner.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.database.PropertyName
import kotlinx.serialization.Serializable

@IgnoreExtraProperties
@Serializable
data class ChallengerRequest(
    var id: String? = null,
    var idOwner: String? = null,
    var description: String? = null,
    var title: String? = null,
    var createDate: String? = null,
    var startDate: String? = null,
    var endDate: String? = null,
    var company: String? = null,
    var minTime: String? = null,
    var maxTime: String? = null,
    var maxDistance: String? = null,
    var minDistance: String? = null,
    var urlImage: String? = null,
    var type: Int? = null,
    var isHeartbeat: Boolean? = null,
    var isLock: Boolean? = null,
    var password: String? = null,
    var group: List<GroupTournament> = listOf()
) {

    constructor() : this(
        "", "", "", "", "", "", "",
        "", "", "", "", "", "", 0, false, false, "", listOf()
    ) {
    }
    @Exclude
    fun toMap(): kotlin.collections.Map<String, Any?> {
        return mapOf(
            "id" to id,
            "idOwner" to idOwner,
            "description" to description,
            "title" to  title,

            "createDate" to createDate,
            "startDate" to startDate,
            "endDate" to endDate,
            "company" to  company,

            "minTime" to minTime,
            "maxTime" to maxTime,
            "maxDistance" to maxDistance,
            "minDistance" to  minDistance,

            "urlImage" to urlImage,
            "type" to type,
            "isHeartbeat" to isHeartbeat,
            "isLock" to  isLock,
            "password" to password,
            "group" to  group,
        )
    }



}