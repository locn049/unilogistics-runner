package com.uni.unilogisticsrunner.model

import com.google.firebase.database.IgnoreExtraProperties
import kotlinx.serialization.Serializable

@IgnoreExtraProperties
@Serializable
data class GroupTournament(
    val id: String? = null,
    var nameGroup: String? = null,
    var runnerJoin: List<InfoRunner>? = null,
    var sumDistance: Double? = null

) {
    constructor() : this(
        "", "", listOf(), 0.0
    ) {
    }
}
@IgnoreExtraProperties
@Serializable
data class InfoRunner(
    var id: String? = null,
    var distance: String? = null,
    var unit: String? = null

) {
    constructor() : this(
        "", "", ""
    ) {
    }
}