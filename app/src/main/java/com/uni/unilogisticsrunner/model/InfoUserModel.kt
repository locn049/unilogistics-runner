package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class InfoUserModel(
    val access_token: String ? = null,
    val athleteModel: AthleteModel ? = null,
    val expires_at: Int ? = null,
    val expires_in: Int ? = null,
    val refresh_token: String ? = null,
    val token_type: String ? = null
)