package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class UserInfo(
    var id: String? = null,
    var email: String? = null,
    var userName: String? = null,
    var address: String? = null,
    var phoneNumber: String? = null,
    var age: Int? = null,
    var heartBeat: Double? = null,
    var gender: Int? = null,
    var sumDistance: Double? = null,
    var infoActivity: InfoActivity? = null
)
