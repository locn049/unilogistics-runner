package com.uni.unilogisticsrunner.model

import com.google.firebase.database.Exclude
import kotlinx.serialization.Serializable

@Serializable
data class InfoUserLogin(
    var id: String? = null,
    var email: String? = null,
    var userName: String? = null,
    var listChallengerJoin: String ?= null


) {
    @Exclude
    fun toMap(): kotlin.collections.Map<String, Any?> {
        return mapOf(
            "email" to email,
            "userName" to userName,
            "listChallengerJoin" to listChallengerJoin,
        )
    }
}