package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class ActivityPer(
    var type: Int? = null,
    var distance: Double? = null
)