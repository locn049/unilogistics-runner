package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class Athlete(
    val id: Int,
    val resource_state: Int
)