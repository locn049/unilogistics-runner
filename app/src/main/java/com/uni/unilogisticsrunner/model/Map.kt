package com.uni.unilogisticsrunner.model

import kotlinx.serialization.Serializable

@Serializable
data class Map(
    val id: String,
    val resource_state: Int,
    val summary_polyline: String
)