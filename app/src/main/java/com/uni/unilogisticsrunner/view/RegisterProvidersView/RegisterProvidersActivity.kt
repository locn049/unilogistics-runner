package com.uni.unilogisticsrunner.view.RegisterProvidersView

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.unilogisticsrunner.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.uni.unilogisticsrunner.model.UserInfo
import com.uni.unilogisticsrunner.ui.theme.TransparentPrimary
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import org.koin.androidx.compose.getViewModel

class RegisterProvidersActivity : AppCompatActivity() {
    companion object
    {
        var userNameLocal = ""
    }
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth

        setContent {
            RegisterView(auth = auth, context = this) {
                onBackPressedDispatcher.onBackPressed()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        onBackPressedDispatcher.onBackPressed()

    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun RegisterView(auth: FirebaseAuth, context: Context,onBackPress: () -> Unit) {
    var userName by remember { mutableStateOf("") }
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var rePassword by remember { mutableStateOf("") }
    var isMatch by remember {
        mutableStateOf(false)
    }
    val viewModel = getViewModel<MainViewViewModel>()

    val keyboardController = LocalSoftwareKeyboardController.current
    //val mainViewViewModel by viewModel.
    Box(Modifier.fillMaxSize()) {

        Image(
            painter = painterResource(id = R.drawable.register_bg),
            contentDescription = "null",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.Crop
        )

        Box(
            Modifier
                .fillMaxSize()
                .background(TransparentPrimary)
                .clickable {
                    keyboardController?.hide()
                }
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
            ) {
                Column(Modifier.fillMaxWidth()) {
                    Text(
                        text = "Đăng kí",
                        fontSize = 38.sp,
                        modifier = Modifier.padding(8.dp),
                        fontWeight = FontWeight.Bold,
//                        color = Color.White
                    )
                    Text(
                        text = "Tạo tài khoản của bạn",
                        Modifier.padding(bottom = 8.dp, start = 8.dp)
                    )
                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .background(Color.White)
//                    .fillMaxHeight()
                ) {

                    Column(Modifier.padding(16.dp)) {
                        OutlinedTextField(
                            value = userName,
                            onValueChange = {
                                userName = it
                            },
                            placeholder = { Text(text = "Tên người dùng") },
                            modifier = Modifier.fillMaxWidth(),
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),

                            )
                        Spacer(modifier = Modifier.height(16.dp))
                        OutlinedTextField(
                            value = email,
                            onValueChange = {
                                email = it
                            },
                            placeholder = { Text(text = "Email") },
                            modifier = Modifier.fillMaxWidth(),
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),

                            )
                        Spacer(modifier = Modifier.height(16.dp))
                        OutlinedTextField(
                            value = password,
                            onValueChange = {
                                password = it
                            },
                            placeholder = { Text(text = "Mật khẩu") },
                            modifier = Modifier.fillMaxWidth(),
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),

                            )

                        AnimatedVisibility(visible = isMatch) {
                            Text(text = "Mật khẩu không khớp", color = Color.Red)
                        }

                        Spacer(modifier = Modifier.height(16.dp))

                        OutlinedTextField(
                            value = rePassword,
                            onValueChange = {
                                rePassword = it
                            },
                            placeholder = { Text(text = "Nhập lại mật khẩu") },
                            modifier = Modifier.fillMaxWidth(),
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                        )

                        Spacer(modifier = Modifier.height(16.dp))



                        Button(
                            onClick = {

                                if (rePassword == password)
                                {
                                    isMatch = false
                                    auth.createUserWithEmailAndPassword(email, password)
                                        .addOnCompleteListener {
                                            if (it.isSuccessful) {
                                                var id = auth.currentUser?.uid.toString()
                                                var userInfo = UserInfo(email = email, userName = userName)
                                                viewModel.createUser(userInfo,id)

                                                onBackPress()
                                            } else {
                                                Log.d("Success", "Errro")

                                            }
                                        }
                                        .addOnFailureListener {

                                        }
                                }
                                else{
                                    isMatch = true
                                }

                            },
                            contentPadding = ButtonDefaults.ContentPadding,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Text(text = "Đăng kí")
                        }

                        Box(Modifier.fillMaxWidth()) {
                            Text(text = "Trở lại đăng nhập", modifier = Modifier.clickable {
                                onBackPress()
                            })
                        }
                    }
                }
            }

        }
    }
}

