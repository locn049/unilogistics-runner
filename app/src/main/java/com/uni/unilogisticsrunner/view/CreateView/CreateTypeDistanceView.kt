package com.uni.unilogisticsrunner.view.CreateView

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.uni.unilogisticsrunner.ui.theme.Pink80
import com.uni.unilogisticsrunner.view.ChanllengeView.ButtonJoin
import com.uni.unilogisticsrunner.view.CreateView.CreateViewActivity.Companion.unit
import com.uni.unilogisticsrunner.view.SplashView.BottomNavItem

@Composable
fun CreateTypeDistanceView(navController: NavController) {
    var isSelected by remember {
        mutableStateOf(false)
    }

    var isChoose by remember {
        mutableStateOf(false)
    }
    var textChange by remember {
        mutableStateOf("")
    }

    Column(
        Modifier
            .fillMaxWidth()
            .padding(16.dp), verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Text(
            text = "Bạn muốn chọn khoảng cách hay thời gian",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
        Text(text = "Mục tiêu của thử thách là giới hạn khoảng cách hoặc thời gian")

        ButtonTypeView(onChange = {
            isSelected = !isSelected
        }, isSelected)
        Column(Modifier.fillMaxWidth()) {
            Text(text = "Chọn mục tiêu", fontSize = 14.sp, fontWeight = FontWeight.Bold)
            Text(text = "Mục tiêu của nhóm tối thiểu cần đạt được để cạnh tranh với nhau.")
        }
        OptionGoalView(if (isSelected) !isChoose else isChoose, onTextChange = {
            textChange = it
        }, text = textChange)

        ButtonJoin(text = "Tiếp tục", onClick = {
            if (isChoose) {
                CreateViewActivity.challengerRequest.minTime = textChange
                unit = "Giờ"
            } else {
                CreateViewActivity.challengerRequest.minDistance = textChange
                unit = "Km"

            }
            navController.navigate(BottomNavItem.CreateDateTime.screen_route)
        })


    }
}

@Composable
fun ButtonTypeView(onChange: (Boolean) -> Unit = {}, isSelected: Boolean) {

    Column(Modifier.fillMaxWidth()) {

        Row(
            Modifier
                .fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(16.dp)

        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth(0.5f)
                    .border(
                        BorderStroke(
                            1.dp,
                            if (isSelected) Pink80 else Color.Gray
                        ), shape = RoundedCornerShape(4.dp)
                    )
                    .background(Color.White)
                    .height(48.dp)
                    .clickable {
                        onChange(isSelected)
                    }
            ) {
                Text(
                    text = "Khoảng cách",
                    color = if (isSelected) Pink80 else Color.Gray,
                    modifier = Modifier.align(Alignment.Center),
                    fontWeight = FontWeight.Bold
                )
            }

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .border(
                        BorderStroke(
                            1.dp,
                            if (!isSelected) Pink80 else Color.Gray
                        ), shape = RoundedCornerShape(4.dp)
                    )
                    .background(Color.White)
                    .height(48.dp)
                    .clickable {
                        onChange(isSelected)
                    }
            ) {
                Text(
                    text = "Thời gian",
                    color = if (!isSelected) Pink80 else Color.Gray,
                    modifier = Modifier.align(Alignment.Center),
                    fontWeight = FontWeight.Bold
                )
            }
        }
    }
}

@Composable
fun OptionGoalView(
    isChoose: Boolean, onTextChange: (String) -> Unit = {},text: String
) {
    var text by remember {
        mutableStateOf(text)
    }


    val context = LocalContext.current
    var expanded by remember { mutableStateOf(false) }
    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(16.dp)) {
        OutlinedTextField(
            value = text,
            onValueChange = {
                text = it
                onTextChange(text)
                            },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            modifier = Modifier
                .fillMaxWidth(0.5f)
                .height(48.dp)
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.TopEnd)
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .border(
                        BorderStroke(
                            1.dp,
                            Color.Gray
                        ), shape = RoundedCornerShape(4.dp)
                    )
                    .background(Color.White)
                    .height(48.dp)
                    .clickable {
                        expanded = !expanded
                    }
            ) {
                Text(
                    text = "Thời gian",
                    color = Color.Gray,
                    modifier = Modifier.align(Alignment.Center),
                    fontWeight = FontWeight.Bold
                )
            }

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .border(
                        BorderStroke(
                            1.dp,
                            Color.Gray
                        ), shape = RoundedCornerShape(4.dp)
                    )
                    .background(Color.White)
                    .height(48.dp)
                    .clickable {
                        expanded = !expanded
                    }
            ) {
                Text(
                    text = if (isChoose) "Kilometer" else "Giờ",
                    color = Color.Gray,
                    modifier = Modifier.align(Alignment.Center),
                    fontWeight = FontWeight.Bold
                )
            }
        }

    }

}

