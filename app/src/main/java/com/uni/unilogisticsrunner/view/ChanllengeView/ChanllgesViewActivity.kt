package com.uni.unilogisticsrunner.view.ChanllengeView

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.view.ActivityView.ActivityViewActivity
import com.uni.unilogisticsrunner.view.CreateView.CreateContentView
import com.uni.unilogisticsrunner.view.CreateView.CreateDateView
import com.uni.unilogisticsrunner.view.CreateView.CreateTypeDistanceView
import com.uni.unilogisticsrunner.view.CreateView.CreateView
import com.uni.unilogisticsrunner.view.CreateView.CreateViewActivity
import com.uni.unilogisticsrunner.view.InfoView.InfoViewActivity
import com.uni.unilogisticsrunner.view.MainView.MainViewActivity
import com.uni.unilogisticsrunner.view.MainView.TopAppBarRunner
import com.uni.unilogisticsrunner.view.RankingView.RankingViewActivity
import com.uni.unilogisticsrunner.view.SettingView.SettingsViewActivity
import com.uni.unilogisticsrunner.view.SplashView.BottomNavItem
import com.uni.unilogisticsrunner.view.SplashView.SplashViewActivity
import com.uni.unilogisticsrunner.viewmodels.CreateViewModel
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun ChallengeViewActivity(navController: NavController) {

    var tabIndex by remember { mutableStateOf(0) }

    val tabs = listOf("Tạo", "Thử thách", "Tham gia")

    Column(modifier = Modifier.fillMaxWidth()) {
        TopAppBarRunner(onClick = {

        })
        TabRow(selectedTabIndex = tabIndex) {
            tabs.forEachIndexed { index, title ->
                Tab(text = { Text(title) },
                    selected = tabIndex == index,
                    onClick = { tabIndex = index })
            }
        }
        when (tabIndex) {
            0 -> CreateTab()
            1 -> ChallengeTab()
            2 -> JoinTab()

        }
    }
}


@Composable
fun ButtonJoin(
    onClick: () -> Unit = {},
    text: String,
    modifier: Modifier = Modifier.fillMaxWidth(),
    color: Color = Color.DarkGray
) {
    Button(
        onClick = {
            onClick()
        },
        colors = ButtonDefaults.buttonColors(containerColor = color),
        modifier = modifier
    )

    {
        Text(text = text, color = Color.White)
    }
}


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun NavigationGraphCreate(navController: NavHostController) {
    NavHost(navController, startDestination = BottomNavItem.CreateType.screen_route) {
        composable(BottomNavItem.CreateType.screen_route) {
            CreateView(navController)
        }
        composable(BottomNavItem.CreateDistance.screen_route) {
            CreateTypeDistanceView(navController)
        }
        composable(BottomNavItem.CreateDateTime.screen_route)
        {
            CreateDateView(navController)
        }
        composable(BottomNavItem.CreateContent.screen_route)
        {
            CreateContentView(navController)
        }
    }
}

