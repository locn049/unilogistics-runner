package com.uni.unilogisticsrunner.view.SplashView

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.unilogisticsrunner.R
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.constants.BundleKey
import com.uni.unilogisticsrunner.constants.BundleKey.CLIENT_ID
import com.uni.unilogisticsrunner.constants.BundleKey.CLIENT_SECRET
import com.uni.unilogisticsrunner.constants.BundleKey.GRANT_TYPE
import com.uni.unilogisticsrunner.constants.Status.STATUS_AUTH
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.view.ActivityView.ActivityViewActivity
import com.uni.unilogisticsrunner.view.ChanllengeView.ChallengeViewActivity
import com.uni.unilogisticsrunner.view.HomeView.HomeViewActivity
import com.uni.unilogisticsrunner.view.InfoView.InfoViewActivity
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity
import com.uni.unilogisticsrunner.view.MainView.MainViewActivity
import com.uni.unilogisticsrunner.view.RankingView.RankingViewActivity
import com.uni.unilogisticsrunner.view.SettingView.SettingsView
import com.uni.unilogisticsrunner.viewmodels.UserDetailViewModel
import kotlinx.coroutines.Job
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashViewActivity : ComponentActivity() {

    private val viewModel: UserDetailViewModel by viewModel()
    var job: Job? = null
    var paramData: HashMap<String, Any> = hashMapOf()
    var pref: UserDataPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            startActivity(Intent(this@SplashViewActivity, LoginProvidersActivity::class.java))
        }
    }
}

@Composable
fun SplashScreen(context: Context) {
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun NavigationGraph(navController: NavHostController) {
    NavHost(navController, startDestination = BottomNavItem.Home.screen_route) {
        composable(BottomNavItem.Home.screen_route) {
            MainViewActivity(navController)
        }
        composable(BottomNavItem.News.screen_route) {
            InfoViewActivity(navController)
        }
        composable(BottomNavItem.Challenge.screen_route) {
            ChallengeViewActivity(navController)
        }
        composable(BottomNavItem.Ranking.screen_route) {
            RankingViewActivity(navController)
        }
        composable(BottomNavItem.Activity.screen_route) {
            ActivityViewActivity(navController)
        }
//        composable(BottomNavItem.Setting.screen_route)
//        {
//            SettingsView(navController = navController)
//        }

    }
}

sealed class BottomNavItem(var title: String, var icon: Int, var screen_route: String) {
    object Home : BottomNavItem("Trang chủ", R.drawable.baseline_home_24, "home_route")
    object News : BottomNavItem("My Network", R.drawable.baseline_newspaper_24, "news_route")
    object Challenge :
        BottomNavItem("Post", R.drawable.baseline_downhill_skiing_24, "challenge_route")

    object Ranking :
        BottomNavItem(
            "Notification",
            R.drawable.baseline_format_list_numbered_24,
            "ranking_route"
        )

    object Activity :
        BottomNavItem("Jobs", R.drawable.baseline_track_changes_24, "activity_route")

    object Setting : BottomNavItem("Cài đặt", R.drawable.baseline_settings_24, "setting_route")
    object CreateType : BottomNavItem("Type", R.drawable.strava_icon, "type_route")
    object CreateDistance : BottomNavItem("Type", R.drawable.strava_icon, "distance_route")
    object CreateDateTime : BottomNavItem("Date", R.drawable.strava_icon, "date_route")

    object CreateContent : BottomNavItem("Content", R.drawable.strava_icon, "content_route")


//    object Create: BottomNavItem("Type",R.drawable.strava_icon,"tye_route")


}