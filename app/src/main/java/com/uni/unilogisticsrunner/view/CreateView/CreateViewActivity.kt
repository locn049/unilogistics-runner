package com.uni.unilogisticsrunner.view.CreateView

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.view.ChanllengeView.NavigationGraphCreate
import com.uni.unilogisticsrunner.view.CreateView.CreateViewActivity.Companion.challengerRequest
import com.uni.unilogisticsrunner.view.SettingView.TopAppBarBack
import com.uni.unilogisticsrunner.view.SplashView.BottomNavItem

class CreateViewActivity : AppCompatActivity() {
    companion object {
        var challengerRequest: ChallengerRequest = ChallengerRequest()
        var unit : String ? = ""
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CreateViewMain()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CreateViewMain() {
    val navController = rememberNavController()

    Scaffold(topBar = {
        TopAppBarBack(onBack = { navController.popBackStack() })
    }) {
        Box(
            modifier = Modifier.padding(it)
        ) {
            NavigationGraphCreate(navController = navController)
        }
    }
}

@Composable
fun CreateView(navController: NavController) {
    Column(
        Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
//        TopAppBarBack()

        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            Text(text = "Starter!", fontSize = 64.sp)
            Text(
                text = "Chọn loại thử thách của bạn.",
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )

            Spacer(modifier = Modifier.height(8.dp))

            CreateItemGroupView(onClick = {
                challengerRequest.type = 1
                navController.navigate(BottomNavItem.CreateDistance.screen_route)

            })
            Spacer(modifier = Modifier.height(16.dp))
            CreateItemPersonView(onClick =
            {
                challengerRequest.type = 2
                navController.navigate(BottomNavItem.CreateDistance.screen_route)
            })
        }

    }
}

@Composable
fun CreateItemGroupView(onClick: () -> Unit = {}) {
    Surface(onClick = {
        onClick()
    }) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .border(border = BorderStroke(0.5.dp, Color.Gray), shape = RoundedCornerShape(6.dp))
        ) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.baseline_groups_2_24),
                    contentDescription = null
                )

                Column(modifier = Modifier.padding(start = 4.dp)) {
                    Text(text = "Thử thách nhóm", fontSize = 16.sp, fontWeight = FontWeight.Bold)
                    Text(
                        text = "Thử thách với nhiều nhóm với nhau. Các đội sẽ thi đấu cùng với nhau.",
                        modifier = Modifier.padding(top = 8.dp)
                    )
                }

            }
        }
    }

}

@Composable
fun CreateItemPersonView(onClick: () -> Unit = {}) {
    Surface(onClick = {
        onClick()
    }) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .border(border = BorderStroke(0.5.dp, Color.Gray), shape = RoundedCornerShape(6.dp))
        ) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.baseline_person_24),
                    contentDescription = null
                )

                Column(modifier = Modifier.padding(start = 4.dp)) {
                    Text(text = "Thử thách cá nhân", fontSize = 16.sp, fontWeight = FontWeight.Bold)
                    Text(
                        text = "Thử thách với nhiều nhóm với nhau. Các đội sẽ thi đấu cùng với nhau.",
                        modifier = Modifier.padding(top = 8.dp)
                    )
                }

            }
        }
    }
}



