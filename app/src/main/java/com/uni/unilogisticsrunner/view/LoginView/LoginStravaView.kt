package com.uni.unilogisticsrunner.view.LoginView

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.constants.BundleKey.APPROVAL_PROMPT
import com.uni.unilogisticsrunner.constants.BundleKey.CLIENT_ID
import com.uni.unilogisticsrunner.constants.BundleKey.CODE
import com.uni.unilogisticsrunner.constants.BundleKey.REDIRECT_URL
import com.uni.unilogisticsrunner.constants.BundleKey.SCOPE
import com.uni.unilogisticsrunner.constants.BundleKey.URL_STRAVA_OAUTH2
import com.uni.unilogisticsrunner.ui.theme.TransparentPrimary

@Composable
fun LoginStravaView() {

    Box(Modifier.fillMaxSize()) {
        Image(
            painter = painterResource(id = R.drawable.runner),
            contentDescription = "null",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillHeight
        )
        Box(
            Modifier
                .fillMaxSize()
                .background(TransparentPrimary)
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .padding(bottom = 36.dp)
                    .align(Alignment.BottomCenter),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Đăng nhập bằng tài khoản Strava",
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )
                LoginButtonStrava()
            }
        }
    }
}

@Composable
fun LoginButtonStrava() {
    val context = LocalContext.current

    Image(
        painter = painterResource(id = R.drawable.btn_strava_connectwith_orange),
        modifier = Modifier.clickable {
            loginStravaCallBack(context)
        },
        contentDescription = null
    )
}

fun loginStravaCallBack(context: Context) {
    val intentUri = Uri.parse(URL_STRAVA_OAUTH2)
        .buildUpon()
        .appendQueryParameter("client_id", CLIENT_ID)
        .appendQueryParameter("redirect_uri", REDIRECT_URL)
        .appendQueryParameter("response_type", CODE)
        .appendQueryParameter("approval_prompt", APPROVAL_PROMPT)
        .appendQueryParameter("scope", SCOPE)
        .build()

    val intent = Intent(Intent.ACTION_VIEW, intentUri)
    context.startActivity(intent)
}

@Composable
fun LoginButtonCustom() {
    val context = LocalContext.current

    Image(
        painter = painterResource(id = R.drawable.btn_strava_connectwith_orange),
        modifier = Modifier.clickable {

        },
        contentDescription = null
    )
}