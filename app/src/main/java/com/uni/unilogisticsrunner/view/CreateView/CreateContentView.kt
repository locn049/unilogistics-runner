package com.uni.unilogisticsrunner.view.CreateView

import android.app.Activity
import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.model.GroupTournament
import com.uni.unilogisticsrunner.model.InfoRunner
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.model.UserInfo
import com.uni.unilogisticsrunner.userLogin
import com.uni.unilogisticsrunner.view.ChanllengeView.ButtonJoin
import com.uni.unilogisticsrunner.view.CreateView.CreateViewActivity.Companion.challengerRequest
import com.uni.unilogisticsrunner.view.CreateView.CreateViewActivity.Companion.unit
import com.uni.unilogisticsrunner.view.SplashView.BottomNavItem
import com.uni.unilogisticsrunner.viewmodels.CreateViewModel
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import com.uni.unilogisticsrunner.viewmodels.UserDetailViewModel
import org.koin.androidx.compose.getViewModel
import java.util.Locale
import java.util.UUID


@Composable
fun CreateContentView(navController: NavController) {

    val viewModel = getViewModel<CreateViewModel>()
    val viewModelUser = getViewModel<MainViewViewModel>()
    var title by remember {
        mutableStateOf("Thử thách A")
    }


    var description by remember {
        mutableStateOf("Mô tả thử thách A")
    }
    val context = LocalContext.current
    val activity = LocalContext.current as Activity

    val pref = UserDataPreferences(context)

    LaunchedEffect(Unit)
    {
        viewModelUser.getUser(pref.getAuthUser().toString())
    }

    val userDetailLogin = viewModelUser.userDetailState.collectAsState()
    val userDetail = viewModelUser.userDetail.collectAsState()


    Column(
        Modifier
            .fillMaxWidth()
            .padding(16.dp), verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Text(
            text = "Tên của thử thách",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
        Text(text = "Có thể chỉnh sửa lại sau.")
        Column(Modifier.fillMaxWidth(), verticalArrangement = Arrangement.spacedBy(8.dp)) {
            Text(
                text = "Tên thử thách",
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )

            OutlinedTextField(
                value = title,
                onValueChange = {
                    title = it
                },
                modifier = Modifier.fillMaxWidth(), singleLine = true
            )
        }



        Column(Modifier.fillMaxWidth(), verticalArrangement = Arrangement.spacedBy(8.dp)) {
            Text(
                text = "Mô tả",
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold
            )

            OutlinedTextField(
                value = description,
                onValueChange = {
                    description = it
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(120.dp), singleLine = false, maxLines = 6
            )
        }

        ButtonJoin(text = "Tạo thử thách", onClick = {


            // var userInfo =

            var group: List<GroupTournament> = listOf(
                GroupTournament(
                    id = createTransactionID(),
                    nameGroup = "Nhóm 1",
                    sumDistance = 0.0,
                    runnerJoin = listOf(
                        InfoRunner(
                            id = pref.getAuthUser(),
                            distance = "0", unit = unit
                        )
                    )
                )
            )

            challengerRequest.title = title
            challengerRequest.description = description
            challengerRequest.idOwner = pref.getAuthUser()
            challengerRequest.group = group
            var id = createTransactionID()
            challengerRequest.id = id

            viewModel.createChallenger(id = id.toString(), challengerRequest = challengerRequest)
            viewModelUser.updateUser(
                id = pref.getAuthUser().toString(), userLogin = InfoUserLogin(
                    email = userDetail.value.email,
                    userName = userDetail.value.userName,
                    listChallengerJoin = id.toString()
                )
            )

            activity?.finish()

//            navController.navigate(BottomNavItem.CreateContent.screen_route)
        })
    }
}

@Throws(Exception::class)
fun createTransactionID(): String? {
    return UUID.randomUUID().toString().replace("-".toRegex(), "").uppercase(Locale.getDefault())
}