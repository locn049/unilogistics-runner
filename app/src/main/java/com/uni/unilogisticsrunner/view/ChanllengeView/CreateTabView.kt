package com.uni.unilogisticsrunner.view.ChanllengeView

import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.view.CreateView.CreateViewActivity
import com.uni.unilogisticsrunner.viewmodels.CreateViewModel
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun CreateTab() {
    val context = LocalContext.current
    val viewModel = getViewModel<CreateViewModel>()
    val viewModelChallenger = getViewModel<MainViewViewModel>()
    val pref = UserDataPreferences(context)
    val userData = viewModelChallenger.userDetail.collectAsState()
    LaunchedEffect(Unit)
    {
        viewModelChallenger.getUser(pref.getAuthUser().toString())
        userData.value.listChallengerJoin?.let { viewModel.getListChallenger(it) }
    }
    val challengerList = viewModel.challengerRequest.collectAsState()

    Column(Modifier.fillMaxWidth()) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Text(
                text = "Tạo Thử Thách",
                Modifier
                    .width(300.dp)
                    .padding(bottom = 16.dp),
                maxLines = 2,
                fontSize = 50.sp

            )

            Text(
                text = "Hãy tạo thử thách để chơi cùng bạn bè. Thử thách với luật lệ và cách thức của bạn và cùng nâng cao sức khỏe nhé !",
                fontSize = 16.sp, modifier = Modifier.padding(bottom = 16.dp)
            )
            ButtonJoin(
                onClick =
                {
                    context.startActivity(Intent(context, CreateViewActivity::class.java))
                }, text = "Tạo thử thách"
            )

        }
        Divider()



        LazyColumn()
        {
            items(challengerList.value)
            {
                if (it != null) {
                    MyChallengesItem(it)
                }

            }
        }

        Divider()


        Box(
            Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Text(
                text = "Các hoạt động đề xuất",
                modifier = Modifier.align(Alignment.CenterStart)
            )

        }

        Divider()

    }
}

@Composable
fun MyChallengesItem(challengerRequest: ChallengerRequest) {
    var isDistance = if (challengerRequest.minDistance?.isEmpty() == true) false else true
    var type = if (isDistance) challengerRequest.minDistance else challengerRequest.minTime
    Row(
        Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.medal_logo),
            contentDescription = null,
            modifier = Modifier.size(48.dp)
        )
        Column(Modifier.fillMaxWidth()) {
            Text(
                text = challengerRequest.title.toString(),
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            )
            Row(Modifier.fillMaxWidth()) {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_bolt_24),
                    contentDescription = null
                )
                Text(text = "0${if (isDistance) "Km" else "Giờ"}/${type}")

            }

        }
    }
}