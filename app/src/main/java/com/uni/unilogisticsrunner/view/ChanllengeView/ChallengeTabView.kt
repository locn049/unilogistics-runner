package com.uni.unilogisticsrunner.view.ChanllengeView

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.utils.goActivity
import com.uni.unilogisticsrunner.viewmodels.CreateViewModel
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChallengeTab() {
    val context = LocalContext.current
    val viewModel = getViewModel<CreateViewModel>()
    var text by rememberSaveable { mutableStateOf("") }
    var active by rememberSaveable { mutableStateOf(false) }
    val challengeDetail = viewModel.challengerAll.collectAsState()
    LaunchedEffect(Unit)
    {
        viewModel.getListChallengerAll()
    }

    Column(
        Modifier
            .fillMaxSize()) {
        Box() {
            SearchBar(
                modifier = Modifier.align(Alignment.TopCenter),
                query = text,
                onQueryChange = { text = it },
                onSearch = { },
                active = active,
                onActiveChange = {
                    active = it
                },
                placeholder = { Text("Hinted search text") },
                leadingIcon = { Icon(Icons.Default.Search, contentDescription = null) },
                trailingIcon = { Icon(Icons.Default.MoreVert, contentDescription = null) },
            ) {
            }
        }

        LazyColumn()
        {
            items(challengeDetail.value)
            {
                if (it != null) {
                   Log.d("dadadadadadada", it.id.toString())

                    ItemChallengerAvailable(it, onJoin = {
                        context.goActivity<ChallengerDetailActivity>(
                            context = context, data = arrayOf(
                                it.id.toString()
                            )
                        )
                    })
                }
            }
        }
    }
}

@Composable
fun ItemChallengerAvailable(challengerRequest: ChallengerRequest, onJoin: () -> Unit = {}) {
    Box(
        Modifier
            .fillMaxWidth()
            .padding(8.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.vector),
            contentDescription = null,
            modifier = Modifier
                .clip(RoundedCornerShape(12.dp))
        )
        Box(
            modifier = Modifier
                .align(Alignment.BottomEnd)
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .padding(16.dp), verticalArrangement = Arrangement.SpaceEvenly
            ) {
                Text(
                    text = challengerRequest.title.toString(),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
                Text(
                    text = challengerRequest.description.toString(),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Medium,
                    modifier = Modifier.width(220.dp)
                )
                ButtonJoin(
                    text = "Tham gia", modifier = Modifier
                        .padding(8.dp), onClick = { onJoin() }
                )
            }
        }


    }
}