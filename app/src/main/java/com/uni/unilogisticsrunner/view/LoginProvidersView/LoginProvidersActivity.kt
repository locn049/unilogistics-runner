package com.uni.unilogisticsrunner.view.LoginProvidersView

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.lifecycleScope
import com.example.unilogisticsrunner.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.constants.BundleKey
import com.uni.unilogisticsrunner.constants.Status
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.ui.theme.TransparentPrimary
import com.uni.unilogisticsrunner.view.HomeView.HomeViewActivity
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity.Companion.emailUser
import com.uni.unilogisticsrunner.view.RegisterProvidersView.RegisterProvidersActivity
import com.uni.unilogisticsrunner.viewmodels.UserDetailViewModel
import kotlinx.coroutines.Job
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.jvm.internal.impl.util.Check

class LoginProvidersActivity : AppCompatActivity() {

    companion object {
        var emailUser: String = ""
        lateinit var auth: FirebaseAuth
        lateinit var firebaseDatabase: FirebaseDatabase
        lateinit var database: DatabaseReference
    }

    private val viewModel: UserDetailViewModel by viewModel()
    var job: Job? = null
    var pref: UserDataPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = Firebase.auth
        database = Firebase.database.reference
        firebaseDatabase = FirebaseDatabase.getInstance();
        pref = UserDataPreferences(this)

        setContent {
            LoginView(auth = auth, this)
        }
    }

    override fun onStart() {
        super.onStart()
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun LoginView(auth: FirebaseAuth, context: Context) {
    var pref = UserDataPreferences(context)
    var paramData: HashMap<String, Any> = hashMapOf()

    val viewModel = getViewModel<UserDetailViewModel>()
    var email by remember { mutableStateOf("locn049@gmail.com") }
    var password by remember { mutableStateOf("123456") }
    var isShow by remember {
        mutableStateOf(false)
    }
    var text by remember {
        mutableStateOf("")
    }
    var isCheck by remember {
        mutableStateOf(true)
    }

    LaunchedEffect(Unit)
    {
        viewModel.checkAuth()
    }

    val checkAuthState = viewModel.checkAuthStateDetail.collectAsState()
    val userDetailState = viewModel.userDetailState.collectAsState()
    val userDetailStateDetail = viewModel.userDetailStateDetail.collectAsState()



    if (checkAuthState.value == Status.STATUS_AUTH) {
        val currentUser = pref.getAuthUser()
        Log.d("currentUser", currentUser.toString())
        if (!currentUser.isNullOrBlank()) {
            context.startActivity(
                Intent(
                    context,
                    HomeViewActivity::class.java
                )
            )
        }
    } else if (checkAuthState.value == Status.STATUS_NO_AUTH) {
        paramData["client_id"] = BundleKey.CLIENT_ID
        paramData["client_secret"] = BundleKey.CLIENT_SECRET
        paramData["grant_type"] = BundleKey.REFRESH_TOKEN
        paramData["refresh_token"] = pref?.getRefreshToken().toString()
        viewModel.getUserDetail(paramData)
    }

    if (userDetailState is DataState.Success<*>) {

        pref.saveToken(userDetailStateDetail.value.access_token.toString())
        pref.saveIdUser(userDetailStateDetail.value.athleteModel?.id.toString())

    }


    val keyboardController = LocalSoftwareKeyboardController.current

    AlertDialogCustomView(openDialog = isShow, onChangeListener = {
        isShow = !it
    }, text = text)

    Box(Modifier.fillMaxSize()) {

        Image(
            painter = painterResource(id = R.drawable.runner),
            contentDescription = "null",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillHeight
        )

        Box(
            Modifier
                .fillMaxSize()
                .background(TransparentPrimary)
                .clickable {
                    keyboardController?.hide()
                }
        ) {
            Column(
                Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
            ) {
                Column(Modifier.fillMaxWidth()) {
                    Text(
                        text = "Đăng nhập",
                        fontSize = 38.sp,
                        modifier = Modifier.padding(8.dp),
                        fontWeight = FontWeight.Bold,
//                        color = Color.White
                    )
                    Text(text = "Chào mừng trở lại", Modifier.padding(bottom = 8.dp, start = 8.dp))
                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                    modifier = Modifier
                        .background(Color.White)
//                    .fillMaxHeight()
                ) {

                    Column(Modifier.padding(16.dp)) {

                        OutlinedTextField(
                            value = email,
                            onValueChange = {
                                email = it
                            },
                            placeholder = { Text(text = "Email") },
                            modifier = Modifier.fillMaxWidth()
                        )
                        Spacer(modifier = Modifier.height(16.dp))
                        OutlinedTextField(
                            value = password,
                            onValueChange = {
                                password = it
                            },
                            placeholder = { Text(text = "Mật khẩu") },
                            modifier = Modifier.fillMaxWidth()
                        )

                        Spacer(modifier = Modifier.height(16.dp))

                        Box(Modifier.fillMaxWidth()) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                modifier = Modifier.align(
                                    Alignment.CenterStart
                                )
                            ) {
                                Checkbox(checked = isCheck, onCheckedChange =
                                {
                                    isCheck = it
                                })
                                Text(text = "Lưu mật khẩu?")
                            }

                            Text(
                                text = "Quên mật khẩu?",
                                modifier = Modifier.align(Alignment.CenterEnd)
                            )
                        }
                        Spacer(modifier = Modifier.height(16.dp))


                        Button(
                            onClick = {
                                auth.signInWithEmailAndPassword(email, password)
                                    .addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            emailUser = email
                                            pref.saveAuthUser(auth.currentUser?.uid.toString())
                                            context.startActivity(
                                                Intent(
                                                    context,
                                                    HomeViewActivity::class.java
                                                )
                                            )

                                        } else {
                                        }
                                    }
                                    .addOnFailureListener {
                                        text = it.localizedMessage?.toString() ?: ""
                                        isShow = true

                                    }
                                    .addOnCanceledListener {
                                        //Log.d("loc123",it.toString())

                                    }
                            },
                            contentPadding = ButtonDefaults.ContentPadding,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Text(text = "Đăng nhập")
                        }
                        Spacer(modifier = Modifier.height(16.dp))

                        Row(Modifier.fillMaxWidth()) {
                            Text(text = "Chưa có tài khoản?")
                            Text(text = "Đăng kí", modifier = Modifier.clickable {
                                context.startActivity(
                                    Intent(
                                        context,
                                        RegisterProvidersActivity::class.java
                                    )
                                )
                            })
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AlertDialogCustomView(
    openDialog: Boolean,
    onChangeListener: (Boolean) -> Unit = {},
    text: String
) {


    if (openDialog) {
        AlertDialog(
            onDismissRequest = {

                onChangeListener(openDialog)

            }
        ) {
            Surface(
                modifier = Modifier
                    .wrapContentWidth()
                    .wrapContentHeight(),
                shape = MaterialTheme.shapes.medium
            ) {
                Column(modifier = Modifier.padding(16.dp)) {


                    Text(
                        text = text,
                    )
                    Spacer(modifier = Modifier.height(24.dp))
                    TextButton(
                        onClick = {
                            onChangeListener(openDialog)
                        },
                        modifier = Modifier.align(Alignment.End)
                    ) {
                        Text("Đóng")
                    }
                }
            }
        }
    }
}
