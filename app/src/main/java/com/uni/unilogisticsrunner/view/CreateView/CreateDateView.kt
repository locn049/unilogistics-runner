package com.uni.unilogisticsrunner.view.CreateView

import android.annotation.SuppressLint
import android.os.Build
import android.view.ContextThemeWrapper
import android.widget.CalendarView
import androidx.annotation.RequiresApi
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.DatePicker
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavController
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.ui.theme.Pink40
import com.uni.unilogisticsrunner.ui.theme.Pink80
import com.uni.unilogisticsrunner.view.ChanllengeView.ButtonJoin
import com.uni.unilogisticsrunner.view.MainView.convertDateTimeToISO8601DateTime
import com.uni.unilogisticsrunner.view.SplashView.BottomNavItem
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.MaterialDialogState
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import com.vanpra.composematerialdialogs.title
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale


@OptIn(ExperimentalMaterial3Api::class)
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CreateDateView(navController: NavController) {

    var isShowDateStart = remember {
        mutableStateOf(false)
    }

    var isShowDateEnd = remember {
        mutableStateOf(false)
    }

    var dialogStateStart = rememberMaterialDialogState()
    var dialogStateEnd = rememberMaterialDialogState()

    var pickerDateStart by remember {
        mutableStateOf(LocalDate.now())
    }
    var pickerDateEnd by remember {
        mutableStateOf(LocalDate.now())
    }

    val formattedDateStart by remember {
        derivedStateOf {
            DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(pickerDateStart)
        }
    }
    val formattedDateEnd by remember {
        derivedStateOf {
            DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(pickerDateEnd)
        }
    }
    val formattedDateNow by remember {
        derivedStateOf {
            DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(LocalDate.now())
        }
    }


    Column(
        Modifier
            .fillMaxWidth()
            .padding(16.dp), verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Text(
            text = "Chọn thời gian để bắt đầu",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
        Text(text = "Và thời gian để hoàn thành")

        Column(Modifier.fillMaxWidth()) {
            Text(text = "Thời gian bắt đầu")

            Surface(
                onClick = {
                    dialogStateStart.show()
                }, modifier = Modifier
                    .padding(top = 8.dp)

                    .fillMaxWidth()
                    .border(width = 0.5.dp, color = Color.Gray, shape = RoundedCornerShape(4.dp))
            ) {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                ) {
                    Text(
                        text = if (!isShowDateStart.value) "Chọn ngày bắt đầu" else formattedDateStart,
                        modifier = Modifier.align(Alignment.CenterStart)
                    )
                    Image(
                        painter = painterResource(id = R.drawable.baseline_arrow_drop_down_24),
                        contentDescription = null,
                        modifier = Modifier.align(Alignment.CenterEnd)
                    )
                }
            }
        }

        AnimatedVisibility(visible = isShowDateStart.value) {
            Column(Modifier.fillMaxWidth()) {
                Text(text = "Thời gian kết thúc")

                Surface(
                    onClick = {
                        dialogStateEnd.show()
                    }, modifier = Modifier
                        .padding(top = 8.dp)

                        .fillMaxWidth()
                        .border(
                            width = 0.5.dp,
                            color = Color.Gray,
                            shape = RoundedCornerShape(4.dp)
                        )
                ) {
                    Box(
                        Modifier
                            .fillMaxWidth()
                            .padding(8.dp)
                    ) {
                        Text(
                            text = if (!isShowDateEnd.value) "Chọn ngày kết thúc" else formattedDateEnd,
                            modifier = Modifier.align(Alignment.CenterStart)
                        )
                        Image(
                            painter = painterResource(id = R.drawable.baseline_arrow_drop_down_24),
                            contentDescription = null,
                            modifier = Modifier.align(Alignment.CenterEnd)
                        )
                    }
                }
            }
        }

        AnimatedVisibility(visible = isShowDateEnd.value) {
            ButtonJoin(text = "Tiếp tục", onClick = {

                CreateViewActivity.challengerRequest.endDate = convertDateTimeToISO8601DateTime(pickerDateStart)

                CreateViewActivity.challengerRequest.startDate = convertDateTimeToISO8601DateTime(pickerDateEnd)

                CreateViewActivity.challengerRequest.createDate = formattedDateNow
                navController.navigate(BottomNavItem.CreateContent.screen_route)
            })
        }


    }

    DatePickerDialogRunner(dialogState = dialogStateStart, onOK = {
        dialogStateStart.hide()
    }, title = "Ngày bắt đầu", onChangeDate = {
        pickerDateStart = it
        isShowDateStart.value = true

    })

    DatePickerDialogRunner(dialogState = dialogStateEnd, onOK = {
        dialogStateStart.hide()
    }, title = "Ngày kết thúc", onChangeDate = {
        pickerDateEnd = it
        isShowDateEnd.value = true

    })
}


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DatePickerDialogRunner(
    onOK: (MaterialDialogState) -> Unit = {},
    dialogState: MaterialDialogState,
    title: String,
    onChangeDate: (LocalDate) -> Unit = {}
) {


    MaterialDialog(dialogState = dialogState, buttons = {
        positiveButton(text = "Ok")
        {
            onOK(dialogState)
        }
        negativeButton(text = "Cancel")
    }
    ) {
        datepicker(initialDate = LocalDate.now(), title = title, allowedDateValidator = {
            it >= LocalDate.now()
        }
        )
        {
            onChangeDate(it)
        }
    }
}



