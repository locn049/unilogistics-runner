package com.uni.unilogisticsrunner.view.ChanllengeView

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.autofill.OnClickAction
import android.util.Log
import android.view.WindowManager
import android.widget.EditText
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.constants.Key
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.model.GroupTournament
import com.uni.unilogisticsrunner.model.InfoRunner
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.model.UserInfo
import com.uni.unilogisticsrunner.view.CreateView.createTransactionID
import com.uni.unilogisticsrunner.viewmodels.ChallengerViewModel
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import org.koin.androidx.compose.getViewModel

class ChallengerDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val data = intent.getStringArrayExtra(Key.BUNDLE)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        setContent {
            DetailView(data)
        }
    }
}

@Composable
fun DetailView(data: Array<String>? = arrayOf()) {
    val idChallenger = data?.component1()
    val viewModel = getViewModel<ChallengerViewModel>()
    var isShowGroup = remember {
        mutableStateOf(false)
    }

    var isShowButtonGroup = remember {
        mutableStateOf(false)
    }

    val context = LocalContext.current
    val pref = UserDataPreferences(context)
    if (idChallenger != null) {
        viewModel.idUserCurrent = idChallenger
    }
    LaunchedEffect(Unit)
    {
        if (idChallenger != null) {
            viewModel.getChallenger(idChallenger)
        }
    }

    val challengerDetail = viewModel.challengerDetailState.collectAsState()
    Box(Modifier.fillMaxWidth()) {
        Column(
            Modifier
                .fillMaxWidth()
                .background(Color.White)
                .align(Alignment.TopCenter)
                .clickable {
                    isShowGroup.value = false
                },
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Box(Modifier.fillMaxWidth()) {
                Image(painter = painterResource(id = R.drawable.runner), contentDescription = null)
                IconButton(
                    onClick = {},
                    modifier = Modifier
                        .align(Alignment.TopStart)
                        .background(Color.White, RoundedCornerShape(99.dp))
                        .size(24.dp)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_arrow_back_24),
                        contentDescription = null, Modifier.size(16.dp)

                    )
                }
            }
            Text(
                text = challengerDetail.value.title.toString(),
                fontSize = 24.sp,
                modifier = Modifier.padding(start = 8.dp)
            )
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_groups_2_24),
                        contentDescription = null
                    )
                    Text(text = "${challengerDetail.value.group.sumOf { it.runnerJoin?.size!! }} thành viên")
                }
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_public_24),
                        contentDescription = null
                    )
                    Text(text = "Public")
                }
                AnimatedVisibility(
                    visible = challengerDetail.value.idOwner == pref.getAuthUser().toString()
                ) {
                    IconButton(onClick = {
                        isShowGroup.value = true
                    }) {
                        Image(
                            painter = painterResource(id = R.drawable.baseline_add_24),
                            contentDescription = null
                        )
                    }
                }

            }

            Text(
                text = challengerDetail.value.description.toString(),
                modifier = Modifier.padding(start = 8.dp)
            )

            TabCustom(challengerRequest = challengerDetail.value, idChallenger = idChallenger)


        }
        AnimatedVisibility(
            visible = isShowGroup.value,
            modifier = Modifier.align(Alignment.BottomCenter)
        ) {
            BottomSheetAddGroup(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White)
                    .align(Alignment.BottomCenter), onCLickClose = {
                    isShowGroup.value = false
                }, onClickCreate = {
                    if (idChallenger != null) {
                        viewModel.addGroup(
                            id = idChallenger,
                            group = GroupTournament(
                                id = createTransactionID(),
                                nameGroup = it,
                                runnerJoin = listOf(),
                                sumDistance = 0.0
                            )
                        )
                    }
                }
            )
        }

    }

}

@Composable
fun TabCustom(challengerRequest: ChallengerRequest, idChallenger: String?) {
    var tabIndex by remember { mutableStateOf(0) }
    val tabs = listOf("Team ranking", "Player Ranking")
    Column(Modifier.fillMaxWidth()) {
        TabRow(selectedTabIndex = tabIndex,
            containerColor = Color(0xff1E76DA),
            modifier = Modifier
                .padding(vertical = 4.dp, horizontal = 8.dp)
                .clip(RoundedCornerShape(99)),
            indicator = {
                Box {

                }
            }
        ) {
            tabs.forEachIndexed { index, text ->
                val selected = tabIndex == index
                Tab(
                    modifier = if (selected) Modifier
                        .clip(RoundedCornerShape(99))
                        .background(
                            Color.White
                        )
                    else Modifier
                        .clip(RoundedCornerShape(99))
                        .background(
                            Color(
                                0xff1E76DA
                            )
                        ),
                    selected = selected,
                    onClick = { tabIndex = index },
                    text = { Text(text = text, color = Color(0xff6FAAEE)) }
                )
            }
        }
        when (tabIndex) {
            0 -> RankTeamView(challengerRequest, idChallenger = idChallenger)
            1 -> RankMyView(challengerRequest, idChallenger = idChallenger)
        }
    }
}

@SuppressLint("StateFlowValueCalledInComposition")
@Composable
fun RankTeamView(
    challengerRequest: ChallengerRequest,
    onClickAction: () -> Unit = {},
    idChallenger: String?
) {

    val context = LocalContext.current
    val pref = UserDataPreferences(context)
    val viewModel = getViewModel<ChallengerViewModel>()
    val challengerDetail = viewModel.challengerDetailState.collectAsState()
    var userCurrent = InfoRunner()
    viewModel.challengerDetailState.value.group.map {
        it.runnerJoin?.forEach { t ->
            if (t.id == pref.getAuthUser()) {
                userCurrent = t
            }
        }
    }

    var text = remember {
        mutableStateOf("")
    }

    Column(
        Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        LazyColumn()
        {
            items(viewModel.challengerDetailState.value.group)
            {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(text = "1", Modifier.weight(0.5f))
                    Row(Modifier.weight(7f), verticalAlignment = Alignment.CenterVertically) {
                        Image(
                            painter = painterResource(id = R.drawable.avatar_sample),
                            contentDescription = null,
                            modifier = Modifier
                                .size(40.dp)
                                .clip(CircleShape),
                            contentScale = ContentScale.Crop

                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        Text(text = it.nameGroup.toString(), color = Color.Gray)
                    }
                    it.runnerJoin?.forEach { info ->
                        if (info.id.equals(
                                pref.getAuthUser().toString()
                            )
                        ) text.value = "Joined" else text.value = "Join"
                    }

                    ButtonJoin(
                        text = text.value,


                        modifier = Modifier.clip(RoundedCornerShape(99.dp)),
                        onClick = {
                            it.runnerJoin?.forEach { info ->
                                if (!info.id.equals(pref.getAuthUser().toString())) {
                                    if (idChallenger != null) {
                                        val data = viewModel.challengerDetailState.value.group
                                        data.forEach { t ->
                                            if (t.id == it.id) {

                                                t.runnerJoin =
                                                    t.runnerJoin?.plus(userCurrent)

                                                data.forEach { t1 ->
                                                    if (t1.id != it.id) {

                                                        t1.runnerJoin?.forEach { info ->
                                                            if (info.id.equals(
                                                                    pref.getAuthUser().toString()
                                                                )
                                                            ) {
                                                                t1.runnerJoin =
                                                                    t1.runnerJoin!! - userCurrent
                                                            }
                                                        }
                                                    }
                                                    viewModel.joinChallenger(
                                                        pref.getAuthUser().toString(),
                                                        id = idChallenger,
                                                        data
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        })
                }
            }
        }
    }
}


@Composable
fun RankMyView(challengerRequest: ChallengerRequest, idChallenger: String?) {
    val viewModel = getViewModel<ChallengerViewModel>()
    val viewModelUser = getViewModel<MainViewViewModel>()
    val context = LocalContext.current
    val pref = UserDataPreferences(context)
    LaunchedEffect(Unit)
    {
        viewModelUser.getAllUser()
    }
    val userDetailState = viewModelUser.allUserDetail.collectAsState()

    var dataUser: List<InfoUserLogin> = listOf()
    var list: List<String> = listOf()

    var userCurrent = InfoRunner()
    challengerRequest.group.forEach {
        it.runnerJoin?.forEach { t ->
            if (t.id == pref.getAuthUser()) {
                list += t.id!!
            }
        }
    }


    userDetailState.value.forEach {
        if (list.contains(it.id)) {
            dataUser += it
        }
    }
    Column(
        Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        LazyColumn()
        {
            items(dataUser)
            {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Text(text = "1", Modifier.weight(0.5f))
                    Row(
                        Modifier.weight(7f),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.avatar_sample),
                            contentDescription = null,
                            modifier = Modifier
                                .size(40.dp)
                                .clip(CircleShape),
                            contentScale = ContentScale.Crop
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        Text(text = it.userName.toString())
                    }
                    Text(text = "999Km", Modifier.weight(2f))
                }
            }
        }
    }
}

private operator fun <T> Array<T>.plusAssign(it: InfoUserLogin) {

}

@Composable
fun BottomSheetAddGroup(
    modifier: Modifier = Modifier.fillMaxWidth(),
    onCLickClose: () -> Unit = {},
    onClickCreate: (String) -> Unit = {}
) {
    var nameGroup = remember {
        mutableStateOf("Nhóm A")
    }
    val viewModel = getViewModel<ChallengerViewModel>()
    Card(modifier = modifier) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp), verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Box(Modifier.fillMaxWidth()) {
                Text(
                    text = "Thêm nhóm",
                    modifier = Modifier.align(Alignment.Center),
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
                IconButton(
                    onClick = {
                        onCLickClose()
                    }, modifier = Modifier.align(Alignment.CenterEnd)
                )
                {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_close_24),
                        contentDescription = null
                    )
                }
            }
            OutlinedTextField(
                value = nameGroup.value,
                onValueChange = {
                    nameGroup.value = it
                },
                placeholder = { Text(text = "Tên nhóm") },
                modifier = Modifier.fillMaxWidth()
            )
            ButtonJoin(text = "Tạo nhóm", modifier = Modifier
                .padding(top = 8.dp)
                .fillMaxWidth(), onClick = {
                onClickCreate(nameGroup.value)
            })
        }
    }
}

