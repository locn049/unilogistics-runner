package com.uni.unilogisticsrunner.view.MainView

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.model.ActivityDetailModel
import com.uni.unilogisticsrunner.ui.theme.Pink80
import com.uni.unilogisticsrunner.utils.extensions.nullToDefault
import com.uni.unilogisticsrunner.view.ChanllengeView.MyChallengesItem
import com.uni.unilogisticsrunner.view.SettingView.SettingsViewActivity
import com.uni.unilogisticsrunner.viewmodels.AthletesViewModel
import com.uni.unilogisticsrunner.viewmodels.CreateViewModel
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import org.koin.androidx.compose.getViewModel
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.TimeZone


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MainViewActivity(navController: NavController) {
    val context = LocalContext.current
    val pref = UserDataPreferences(context)
    val viewModelAthletes = getViewModel<AthletesViewModel>()
    val viewModel = getViewModel<CreateViewModel>()
    val viewModelChallenger = getViewModel<MainViewViewModel>()
    val userData = viewModelChallenger.userDetail.collectAsState()
    LaunchedEffect(Unit)
    {
        viewModelChallenger.getUser(pref.getAuthUser().toString())
        userData.value.listChallengerJoin?.let { viewModel.getListChallenger(it) }
    }
    val challengerList = viewModel.challengerRequest.collectAsState()
    val athletesDetailState by viewModelAthletes.userDetailState.collectAsState()
    val athleteDetail by viewModelAthletes.userDetail.collectAsState()

    val activityDetailState by viewModelAthletes.activityDetailState.collectAsState()
    val activityDetail by viewModelAthletes.activityDetail.collectAsState()


    when (athletesDetailState) {
        is DataState.Success -> {

        }

        else -> {}
    }

    LaunchedEffect(Unit)
    {
        viewModelAthletes.getAthletes()
        viewModelAthletes.getListActivity()
    }

    Column(
        Modifier
            .fillMaxSize()
    ) {
        TopAppBarRunner(onClick = {
            context.startActivity(Intent(context, SettingsViewActivity::class.java))
            //navController.navigate(BottomNavItem.Setting.screen_route)
        })
        Column(Modifier.padding(8.dp)) {

            Box(Modifier.fillMaxWidth()) {
                Text(
                    text = "Hoạt động trong tuần",
                    modifier = Modifier.align(Alignment.CenterStart)
                )
                TextButton(onClick = {}, modifier = Modifier.align(Alignment.CenterEnd)) {
                    Text(text = "Xem hết")
                }

            }
            LazyColumn()
            {
                items(activityDetail)
                {
                    ActivityItem(it)
                }
            }
            Box(Modifier.fillMaxWidth()) {
                Text(
                    text = "Thử thách đang tham gia",
                    modifier = Modifier.align(Alignment.CenterStart)
                )
                TextButton(onClick = {}, modifier = Modifier.align(Alignment.CenterEnd)) {
                    Text(text = "Xem hết")
                }
            }
//
            LazyColumn()
            {
                items(challengerList.value)
                {
                    if (it != null) {
                        MyChallengesItem(it)
                    }
                }
            }

        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun ActivityItem(activityDetailModel: ActivityDetailModel) {
    Surface(modifier = Modifier
        .fillMaxWidth()
        .clickable {

        }) {
        Row(Modifier.fillMaxWidth()) {
            Image(
                painter = painterResource(id = R.drawable.medal_logo),
                contentDescription = null,
                modifier =
                Modifier.size(80.dp)
            )
            Column {
                Text(text = convertISO8601DateTime(activityDetailModel.start_date_local))
                Text(text = "${changeMeterToKilometer(activityDetailModel.distance.nullToDefault())} KM")
                Text(text = "Bạn đã hoạt động được ${activityDetailModel.moving_time} giây")
            }
        }
    }

}

@SuppressLint("StateFlowValueCalledInComposition")
@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun TopAppBarRunner(onClick: () -> Unit = {}) {
    val viewModel = getViewModel<AthletesViewModel>()

    val athleteData by viewModel.userDetail.collectAsState()

    Row(
        Modifier
            .fillMaxWidth()
            .background(Pink80)
            .padding(8.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        GlideImage(
            model = viewModel.userDetail.value?.profile,
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(52.dp)
                .clip(CircleShape),

            )

        Column {
            Text(
                text = athleteData?.lastname + " " + athleteData?.firstname,
                maxLines = 2,
                modifier = Modifier
                    .width(100.dp)
                    .padding(start = 8.dp),
                fontSize = 16.sp,
                fontWeight = FontWeight.Medium
            )
            Text(text = "Total: 100Km", modifier = Modifier.padding(start = 8.dp), fontSize = 12.sp)
        }
        Box(Modifier.fillMaxWidth()) {
            Row(modifier = Modifier.align(Alignment.BottomEnd)) {
                IconButton(onClick = { }) {
                    Image(
                        painter = painterResource(id = R.drawable.baseline_notifications_24),
                        contentDescription = null
                    )
                }
                IconButton(onClick = { onClick() }) {
                    Image(
                        painter = painterResource(id = R.drawable.baseline_settings_24),
                        contentDescription = null
                    )
                }
            }
        }
    }
}

fun changeMeterToKilometer(meter: Double): Double =
    (meter / 1000).toBigDecimal().setScale(2, RoundingMode.DOWN).toDouble()


@SuppressLint("SimpleDateFormat")
@RequiresApi(Build.VERSION_CODES.O)
fun convertISO8601DateTime(isoDateTime: String?): String {
    val odt: OffsetDateTime = OffsetDateTime.parse(isoDateTime)
    val instant: Instant = odt.toInstant()
    val myDate = Date.from(instant)
    val formatter = SimpleDateFormat("dd/MM/yyyy")
    val formattedDate = formatter.format(myDate)
    return formattedDate.toString()
}

@SuppressLint("SimpleDateFormat")
@RequiresApi(Build.VERSION_CODES.O)
fun convertDateTimeToISO8601DateTime(localDate: LocalDate): String {
    val dtf = DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ssX")
    // return date.atStartOfDay().atOffset(ZoneOffset.UTC).toString();
    return localDate.atStartOfDay().atOffset(ZoneOffset.UTC).format(dtf)
}


