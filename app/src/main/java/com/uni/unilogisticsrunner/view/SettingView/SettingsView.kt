package com.uni.unilogisticsrunner.view.SettingView

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.lifecycleScope
import com.example.unilogisticsrunner.R
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.constants.BundleKey
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.ui.theme.Pink80
import com.uni.unilogisticsrunner.view.LoginView.loginStravaCallBack
import com.uni.unilogisticsrunner.view.SplashView.SplashViewActivity
import com.uni.unilogisticsrunner.viewmodels.UserDetailViewModel
import kotlinx.coroutines.Job
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class SettingsViewActivity : AppCompatActivity() {
    private val viewModel: UserDetailViewModel by viewModel()
    var job: Job? = null
    var paramData: HashMap<String, Any> = hashMapOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SettingsView(onBack = { onBackPressed() })
        }
        val pref = UserDataPreferences(this)

        val appLinkIntent: Intent = intent
        val appLinkAction: String? = appLinkIntent.action
        val appLinkData: Uri? = appLinkIntent.data

        job = lifecycleScope.launchWhenResumed {
            viewModel.userDetailState.collect { dataState ->
                when (dataState) {
                    is DataState.Loading -> {

                    }

                    is DataState.Success -> {
                        pref.saveToken(dataState.data.access_token.toString())
                        pref.saveIdUser(dataState.data.athleteModel?.id.toString())
                    }

                    is DataState.Error -> {
                        Log.d("data123", dataState.exception.toString())

                    }

                    else -> {

                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val uri = intent?.data
    }

    override fun onResume() {
        super.onResume()
        val intent = intent
        val uri = intent.data
        val code = uri?.getQueryParameter("code")
        if (uri != null) {
            if (uri.getQueryParameter("code") != null) {
                val code = uri.getQueryParameter("code")
                paramData["client_id"] = BundleKey.CLIENT_ID
                paramData["client_secret"] = BundleKey.CLIENT_SECRET
                paramData["code"] = code.toString()
                paramData["grant_type"] = BundleKey.GRANT_TYPE
                viewModel.getUserDetail(paramData)

            } else {

            }
        }
    }

}


@Composable
fun SettingsView(onBack: () -> Unit = {}) {
    val context = LocalContext.current
    val viewModel = getViewModel<UserDetailViewModel>()
    val userDetailState = viewModel.userDetailState.collectAsState()
    val pref = UserDataPreferences(context)
    var isConnect = remember {
        mutableStateOf("Not connect")
    }

    if (userDetailState is DataState.Success<*> || pref.getToken()?.isNotEmpty() == true)
    {
        isConnect.value= "Conntected"
    }

    Column(
        Modifier
            .fillMaxSize()
            .background(Color.White)) {
        TopAppBarBack(onBack = { onBack() })
        Text(text = "Liên kết ứng dụng")
        Text(text = isConnect.value)

        TextButton(onClick = {
            loginStravaCallBack(context)
        }) {
            Image(painter = painterResource(id = R.drawable.strava_icon), contentDescription = null)
        }

        TextButton(onClick = {
            pref.logout()
            val intent = Intent(context, SplashViewActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
            //context.finish() // if the activity running has it's own context


//            context.startActivity(Intent(context, SplashViewActivity::class.java))
        }) {
            Text(text = "Đăng xuất")
        }
    }
}

@Composable
fun TopAppBarBack(onBack: () -> Unit = {}) {
    Column(
        Modifier
            .fillMaxWidth()
            .background(Pink80)) {
        IconButton(onClick = { onBack() }) {
            Image(
                painter = painterResource(id = R.drawable.baseline_arrow_back_24),
                contentDescription = null
            )
        }
    }
}