package com.uni.unilogisticsrunner.constants

object BundleKey {
   const val URL_STRAVA_OAUTH2 = "https://www.strava.com/oauth/mobile/authorize"
    const val REDIRECT_URL = "http://ivfphuongchau.com/"
    const val CLIENT_ID = "108009"
    const val CODE = "code"
    const val APPROVAL_PROMPT = "auto"
    const val SCOPE = "activity:read_all,activity:write"
    const val GRANT_TYPE = "authorization_code"
    const val REFRESH_TOKEN = "refresh_token"
    const val CLIENT_SECRET = "b01be7af9e4ea372e986563c38750d8d38d9be03"
}

object Key{
    const val BUNDLE = "BUNDLE"
}
object Status
{
    const val STATUS_NO_AUTH = "status_no_auth"
    const val STATUS_AUTH = "status_auth"
}

object ServerErrors {
    const val NONE = "NONE"

    //const val submitting = "SUBMITTING"
    const val SUCCESS = "SUCCESS"
    const val ERROR = "ERROR"
    const val WARNING = "WARNING"
    const val LOADING = "LOADING"

    //const val progress = "PROGRESS"
    const val EXISTED = "EXISTED"
    const val INTERNET_ERROR = "INTERNET_ERROR"
}
