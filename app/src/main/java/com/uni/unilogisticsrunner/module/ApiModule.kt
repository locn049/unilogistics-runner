package com.uni.unilogisticsrunner.module

import android.content.Context
import android.util.Log
import com.uni.unilogisticsrunner.api.ApiServices
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.repository.FirebaseRepository
import com.uni.unilogisticsrunner.repository.AthletesRepository
import com.uni.unilogisticsrunner.repository.CreateRepository
import com.uni.unilogisticsrunner.repository.UserDetailRepository
import com.uni.unilogisticsrunner.viewmodels.AthletesViewModel
import com.uni.unilogisticsrunner.viewmodels.ChallengerViewModel
import com.uni.unilogisticsrunner.viewmodels.CreateViewModel
import com.uni.unilogisticsrunner.viewmodels.MainViewViewModel
import com.uni.unilogisticsrunner.viewmodels.UserDetailViewModel

import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.client.*
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.DefaultRequest
import io.ktor.client.features.HttpTimeout
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.ANDROID
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.headers
import kotlinx.serialization.json.Json

import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    fun initKtorClient(context: Context) = HttpClient(OkHttp) {
        engine {
            config {
                // this: OkHttpClient.Builder
                followRedirects(true)
            }
        }

        val json = Json {
            isLenient = true
            encodeDefaults = true
            coerceInputValues = true
            ignoreUnknownKeys = true
        }
        install(JsonFeature) {
            serializer = KotlinxSerializer(json)
        }

        install(DefaultRequest) {
            contentType(ContentType.Application.Json)
            headers {
                append("Content-Type", "application/json; charset=UTF-8")
            }
        }
        install(HttpTimeout) {
            socketTimeoutMillis = 30_00000
            requestTimeoutMillis = 30_00000
            connectTimeoutMillis = 30_00000
        }

        install(Logging) {
            logger = object : Logger {
                override fun log(message: String) {
                    Log.i("Network", message)
                }
            }
            logger = Logger.ANDROID
            level = LogLevel.ALL
        }
    }


    factory { ApiServices(get(), get()) }
    single { initKtorClient(androidContext()) }
    single { UserDataPreferences(get()) }

    single { UserDetailRepository(get()) }
    single { FirebaseRepository() }
    single { AthletesRepository(get()) }
    single { CreateRepository() }

    viewModel { UserDetailViewModel(get()) }
    viewModel { MainViewViewModel(get()) }
    viewModel { AthletesViewModel(get()) }
    viewModel { CreateViewModel(get()) }
    viewModel { ChallengerViewModel()}

}


