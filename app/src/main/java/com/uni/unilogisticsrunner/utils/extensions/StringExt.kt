package com.uni.unilogisticsrunner.utils.extensions

fun <R> CharSequence?.isTextEmpty(block: (CharSequence?) -> R): R? {
    return if (this == null || this.trim().isEmpty()) block(this) else null
}

fun CharSequence?.isTextEmpty(): Boolean {
    return this == null || this.trim().isEmpty()
}

fun CharSequence?.isTextNotEmpty(): Boolean {
    return !isTextEmpty()
}

fun <R> String?.isTextNotEmpty(block: (String) -> R): R? {
    return if (this == null || this.trim().isEmpty()) null else block(this)
}

fun String?.nullToNone(): String = this ?: "None"
fun String.toIntOrZero() = if (this.toIntOrNull() == null) 0 else this.toInt()
fun String.toIntOrNegative() = if (this.toIntOrNull() == null) -1 else this.toInt()

fun String?.nullToEmpty(): String = this ?: ""

