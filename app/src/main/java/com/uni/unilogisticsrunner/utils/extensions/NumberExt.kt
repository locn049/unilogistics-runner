package com.uni.unilogisticsrunner.utils.extensions

fun Int?.nullToDefault() = this ?: -1

fun Int?.nullToZero() = this ?: 0
fun Int?.nullToZeroString() = this.nullToZero().toString()
fun Int?.nullToNegative() = this ?: -1
fun Int?.gtZero() = this != null && this > 0

fun Int?.isTrue() = this == 1

fun Double?.isNullOrZero() = this == 0.0 || this == null

fun Double?.nullToDefault() = this ?: -1.0

fun Double?.nullToZero() = this ?: 0.0
