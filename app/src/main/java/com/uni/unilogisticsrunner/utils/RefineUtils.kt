package com.uni.unilogisticsrunner.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.uni.unilogisticsrunner.constants.Key.BUNDLE

infix fun <T> Collection<T>.deepEqualTo(other: Collection<T>): Boolean {
    // check collections aren't same
    if (this !== other) {
        // fast check of sizes
        if (this.size != other.size) return false
        val areNotEqual = this.asSequence()
            .zip(other.asSequence())
            .map { (fromThis, fromOther) -> fromThis == fromOther }
            .contains(false)
        if (areNotEqual) return false
    }
    return true
}

inline fun <reified T : Activity> Context.goActivity(
    context: Context,
    vararg data: String
) {
    val intent = Intent(context, T::class.java)
    intent.putExtra(BUNDLE,data)
    startActivity(intent)
}

