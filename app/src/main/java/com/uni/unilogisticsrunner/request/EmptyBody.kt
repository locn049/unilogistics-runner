package com.uni.unilogisticsrunner.request

import kotlinx.serialization.Serializable

@Serializable
 class EmptyBody()