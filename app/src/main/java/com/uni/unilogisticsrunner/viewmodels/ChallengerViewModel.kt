package com.uni.unilogisticsrunner.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue
import com.uni.unilogisticsrunner.base.AlertState
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.model.GroupTournament
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class ChallengerViewModel() : ViewModel() {
    var alertState = MutableStateFlow<AlertState>(AlertState.Unknown)
    var challengerDetailState =
        MutableStateFlow<ChallengerRequest>(ChallengerRequest())
    var idUserCurrent: String = ""

    fun getChallenger(id: String) {
        viewModelScope.launch {
            val uidRef = LoginProvidersActivity.database.ref.child("challenger").child(id)
            uidRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        challengerDetailState.value =
                            snapshot.getValue(ChallengerRequest::class.java)
                                ?: ChallengerRequest()
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                }
            })
        }
    }

    fun joinChallenger(idUser: String, id: String, group: List<GroupTournament>) {
        viewModelScope.launch {
            LoginProvidersActivity.database.child("challenger").child(id).child("group")
                .setValue(group)
                .addOnCompleteListener {
                    getChallenger(idUserCurrent)
                }.addOnFailureListener {
                    Log.d("firebaseUpdate", "Error")
                }
        }
    }

    fun addGroup(id: String, group: GroupTournament) {
        viewModelScope.launch {
            var ref = LoginProvidersActivity.database.child("challenger").child(id).child("group").
            ref.setValue(group)

        }
    }
}