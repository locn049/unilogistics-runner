package com.uni.unilogisticsrunner.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.uni.unilogisticsrunner.base.AlertState
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.model.InfoUserModel
import com.uni.unilogisticsrunner.model.UserInfo
import com.uni.unilogisticsrunner.repository.FirebaseRepository
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class MainViewViewModel(private val firebaseRepository: FirebaseRepository) :
    ViewModel() {

    var alertState = MutableStateFlow<AlertState>(AlertState.Unknown)
    var userDetailState =
        MutableStateFlow<DataState<InfoUserLogin>>(DataState.InitNothing)

    var userDetail = MutableStateFlow<InfoUserLogin>(InfoUserLogin())
    var allUserDetail = MutableStateFlow<List<InfoUserLogin>>(listOf())

    fun createUser(userInfo: UserInfo, id: String) {
        viewModelScope.launch {
            firebaseRepository.createUser(userInfo, id)
        }
    }

    fun updateUser(userLogin: InfoUserLogin, id: String) {
        LoginProvidersActivity.database.child("users").child(id).setValue(userLogin)
            .addOnCompleteListener {
                Log.d("firebaseUpdate","Success")
            }.addOnFailureListener {
                Log.d("firebaseUpdate","Error")

        }
    }

    fun getUser(id: String) {
        viewModelScope.launch {
            var uidRef = LoginProvidersActivity.database.ref.child("users").child(id)
            uidRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    userDetail.value = dataSnapshot.getValue(InfoUserLogin::class.java)!!
                }

                override fun onCancelled(error: DatabaseError) {
                    //alertState.value = AlertState.Error(message = error.message, code = error.code)

                }
            })
        }
    }

    fun getAllUser()
    {
        viewModelScope.launch {
            var uidRef = LoginProvidersActivity.database.ref.child("users")
            uidRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    allUserDetail.value = listOf()
                    for (postSnapshot in dataSnapshot.children) {
                        val allUserDetailState =
                            postSnapshot.getValue(InfoUserLogin::class.java)
                        allUserDetail.value += allUserDetailState!!
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    //alertState.value = AlertState.Error(message = error.message, code = error.code)

                }
            })
        }
    }


}
