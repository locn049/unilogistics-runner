package com.uni.unilogisticsrunner.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uni.unilogisticsrunner.base.AlertState
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.model.ActivityDetailModel
import com.uni.unilogisticsrunner.model.AthleteActivityModel
import com.uni.unilogisticsrunner.model.InfoUserModel
import com.uni.unilogisticsrunner.repository.AthletesRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ViewModelOwnerDefinition

class AthletesViewModel(private val athletesRepository: AthletesRepository) : ViewModel() {
    var alertState = MutableStateFlow<AlertState>(AlertState.Unknown)
    var userDetailState =
        MutableStateFlow<DataState<AthleteActivityModel>>(DataState.InitNothing)

    var userDetail = MutableStateFlow<AthleteActivityModel>(AthleteActivityModel())

    var activityDetailState =
        MutableStateFlow<DataState<List<ActivityDetailModel>>>(DataState.InitNothing)

    var activityDetail = MutableStateFlow<List<ActivityDetailModel>>(listOf())

    fun getAthletes() {
        viewModelScope.launch {
            athletesRepository.getAthlete().collect {
                userDetailState.value = it
                when (it) {
                    is DataState.Success -> {
                        userDetail.value = it.data
                    }

                    is DataState.Error -> {
                    }

                    else -> {}
                }
            }
        }
    }

    fun getListActivity() {
        viewModelScope.launch {
            athletesRepository.getListActivity().collect {
                activityDetailState.value = it
                when (it) {
                    is DataState.Success -> {
                        activityDetail.value = it.data
                    }

                    is DataState.Error -> {
                    }

                    else -> {}
                }
            }
        }
    }
}