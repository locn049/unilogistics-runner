package com.uni.unilogisticsrunner.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.childEvents
import com.google.firebase.database.ktx.getValue
import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.repository.CreateRepository
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch


class CreateViewModel(private val createRepository: CreateRepository) : ViewModel() {

    var challengerRequest = MutableStateFlow<List<ChallengerRequest?>>(listOf())
    var challengerAll = MutableStateFlow<List<ChallengerRequest?>>(listOf())

    fun createChallenger(id: String, challengerRequest: ChallengerRequest) {
        viewModelScope.launch {
            createRepository.createChallenger(id, challengerRequest)
        }
    }

    fun getListChallenger(listId: String) {
        viewModelScope.launch {
            var uidRef = LoginProvidersActivity.database.ref.child("challenger")
            uidRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    challengerRequest.value = listOf()
                    for (postSnapshot in dataSnapshot.children) {

                        if (postSnapshot.key == listId) {
                            val challengerRequestData =
                                postSnapshot.getValue(ChallengerRequest::class.java)
                            challengerRequest.value += challengerRequestData
                        }

                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
        }
    }

    fun getListChallengerAll() {
        viewModelScope.launch {
            var uidRef = LoginProvidersActivity.database.ref.child("challenger")
            uidRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    challengerAll.value = listOf()
                    for (postSnapshot in dataSnapshot.children) {
                        val challengerRequestData =
                            postSnapshot.getValue(ChallengerRequest::class.java)
                        challengerAll.value += challengerRequestData
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
        }
    }


}