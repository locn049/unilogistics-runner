package com.uni.unilogisticsrunner.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uni.unilogisticsrunner.base.AlertState
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.model.InfoUserModel
import com.uni.unilogisticsrunner.repository.UserDetailRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class UserDetailViewModel(private val userDetailRepository: UserDetailRepository) :
    ViewModel() {

    var userDetailState =
        MutableStateFlow<DataState<InfoUserModel>>(DataState.InitNothing)
    var userDetailStateDetail =
        MutableStateFlow<InfoUserModel>(InfoUserModel())

    var alertState = MutableStateFlow<AlertState>(AlertState.Unknown)

    var checkAuthState = MutableStateFlow<DataState<String>>(DataState.InitNothing)
    var checkAuthStateDetail = MutableStateFlow<String>("")


    fun getUserDetail(paramData: HashMap<String, Any>) {
        viewModelScope.launch {
            userDetailRepository.getUserDetail(paramData).collect {
                userDetailState.value = it
                when (it) {
                    is DataState.Success -> {
                        userDetailStateDetail.value = it.data
                    }

                    is DataState.Error -> {
                    }

                    else -> {}
                }
            }
        }
    }

    fun checkAuth() {
        viewModelScope.launch {
            userDetailRepository.checkAuth().collect {
                checkAuthState.value = it
                when (it) {
                    is DataState.Success -> {
                        checkAuthStateDetail.value = it.data
                    }

                    is DataState.Error -> {
                    }

                    else -> {}
                }
            }
        }
    }
}