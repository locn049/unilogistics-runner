package vn.unilogistics.bwms.api

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class JsonHelper {
    val json = Json {
        ignoreUnknownKeys = true
        //prettyPrint = true
        isLenient = true
        coerceInputValues = true
    }

    inline fun <reified T> decodeFromString(jsonString: String): T {
        return json.decodeFromString(jsonString)
    }

    inline fun <reified T> encodeToString(obj: T): String {
        return Json.encodeToString(obj)
    }
}