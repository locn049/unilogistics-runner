package com.uni.unilogisticsrunner.api

import com.uni.unilogisticsrunner.utils.SettingsUtils

val buildUrl = { apiName: ApiUrl.ApiName, requestLink: String? ->
    var url = if (requestLink.isNullOrEmpty()) {
        ApiUrl.getLink(apiName)
    } else {
        SettingsUtils.baseURL + requestLink
    }
    url
}

class ApiUrl {
    enum class ApiName {
        UserDetailLogin,
        AthletesDetail,
        ListActivity,
        RefreshToken
    }

    companion object {
        private const val UserDetailLogin = "/oauth/token"
        private const val AthletesDetail = "/athlete"
        private const val ListActivity = "/athlete/activities"


        fun getLink(name: ApiName): String {
            var link = ""

            when (name) {
                ApiName.UserDetailLogin -> link = UserDetailLogin
                ApiName.AthletesDetail -> link = AthletesDetail
                ApiName.ListActivity -> link = ListActivity

                else -> {}
            }

            return SettingsUtils.baseURL + link
        }
    }
}