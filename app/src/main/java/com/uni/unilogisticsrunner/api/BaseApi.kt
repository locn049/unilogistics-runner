package com.uni.unilogisticsrunner.api

import io.ktor.client.*
import com.uni.unilogisticsrunner.manager.UserDataPreferences

open class BaseApi(val httpClient: HttpClient, val pref: UserDataPreferences) {
    val accessToken: String?
        get() {
            return pref.getToken()
        }
}