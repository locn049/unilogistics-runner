package com.uni.unilogisticsrunner.api

import com.uni.unilogisticsrunner.constants.Status.STATUS_AUTH
import com.uni.unilogisticsrunner.constants.Status.STATUS_NO_AUTH
import com.uni.unilogisticsrunner.manager.UserDataPreferences
import com.uni.unilogisticsrunner.request.EmptyBody
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.headers
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.util.InternalAPI
import vn.unilogistics.bwms.api.JsonHelper

class ApiServices(httpClient: HttpClient, pref: UserDataPreferences) :
    BaseApi(httpClient = httpClient, pref = pref) {


    suspend inline fun <reified T> checkAuth(url: String): String {
        try {
            val response: HttpResponse = httpClient.get(url) {
                headers {
                    append("Authorization", "Bearer $accessToken")
                }
            }

            if (response.status.value.equals(401)) {
                return STATUS_NO_AUTH
            } else return STATUS_AUTH

        } catch (e: Exception) {
            return JsonHelper().decodeFromString(e.toString())
        }
    }

    suspend inline fun <reified T> get(url: String): T {
        try {
            val response: HttpResponse = httpClient.get(url) {
                headers {
                    append("Authorization", "Bearer $accessToken")
                }
            }
            return JsonHelper().decodeFromString(response.readText())
        } catch (e: Exception) {

            return JsonHelper().decodeFromString(e.toString())
        }
    }

    @OptIn(InternalAPI::class)
    suspend inline fun <reified T> post(
        url: String,
        body: Any = EmptyBody(),
        param: HashMap<String, Any>? = null
    ): T {
        val response: HttpResponse = httpClient.post(url) {
            headers {
                append("Authorization", "Bearer $accessToken")
            }
            this.body = body
            if (param?.isNotEmpty() == true) {
                param.forEach { (k, v) ->
                    parameter(k, v)
                }
            }
        }

        return JsonHelper().decodeFromString(response.readText())
    }

}

