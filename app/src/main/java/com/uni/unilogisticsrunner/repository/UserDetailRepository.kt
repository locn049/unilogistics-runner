package com.uni.unilogisticsrunner.repository

import android.util.Log
import com.uni.unilogisticsrunner.api.ApiServices
import com.uni.unilogisticsrunner.api.ApiUrl
import com.uni.unilogisticsrunner.api.buildUrl
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.model.ActivityDetailModel
import com.uni.unilogisticsrunner.model.InfoUserModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UserDetailRepository(private val apiServices: ApiServices) {
    fun getUserDetail(paramData: HashMap<String, Any>): Flow<DataState<InfoUserModel>> = flow {
        emit(DataState.Loading)
        val url = String.format(buildUrl(ApiUrl.ApiName.UserDetailLogin, null))
        try {
            val response: InfoUserModel = apiServices.post(
                url, param = paramData
            )
            Log.d("data123", response.toString())
            emit(DataState.Success(response))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    fun checkAuth(): Flow<DataState<String>> = flow {
        emit(DataState.Loading)
        val url = String.format(buildUrl(ApiUrl.ApiName.ListActivity, null))
        try {
            val response: String = apiServices.checkAuth<String>(
                url
            )
            Log.d("getAthlete", response.toString())
            emit(DataState.Success(response))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}