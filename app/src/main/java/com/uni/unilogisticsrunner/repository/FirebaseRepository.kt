package com.uni.unilogisticsrunner.repository

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.model.InfoUserModel
import com.uni.unilogisticsrunner.model.UserInfo
import com.uni.unilogisticsrunner.request.EmptyBody
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity.Companion.database
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import okhttp3.Dispatcher


class FirebaseRepository() {

    fun createUser(userInfo: UserInfo, id: String) {
        database.child("users").child(id).setValue(userInfo)
    }


}