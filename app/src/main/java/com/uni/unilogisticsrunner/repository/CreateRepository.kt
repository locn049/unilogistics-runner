package com.uni.unilogisticsrunner.repository

import com.uni.unilogisticsrunner.model.ChallengerRequest
import com.uni.unilogisticsrunner.model.InfoUserLogin
import com.uni.unilogisticsrunner.view.LoginProvidersView.LoginProvidersActivity.Companion.database
class CreateRepository() {

    fun createChallenger(id: String,challengerRequest: ChallengerRequest)
    {
        database.child("challenger").child(id).setValue(challengerRequest)
    }

}