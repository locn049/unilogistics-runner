package com.uni.unilogisticsrunner.repository

import android.util.Log
import com.uni.unilogisticsrunner.api.ApiServices
import com.uni.unilogisticsrunner.api.ApiUrl
import com.uni.unilogisticsrunner.api.buildUrl
import com.uni.unilogisticsrunner.base.DataState
import com.uni.unilogisticsrunner.model.ActivityDetailModel
import com.uni.unilogisticsrunner.model.AthleteActivityModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class AthletesRepository(private val apiServices: ApiServices) {

    fun getAthlete(): Flow<DataState<AthleteActivityModel>> = flow {
        emit(DataState.Loading)
        val url = String.format(buildUrl(ApiUrl.ApiName.AthletesDetail, null))
        try {
            val response: AthleteActivityModel = apiServices.get(
                url
            )
            Log.d("getAthlete", response.toString())
            emit(DataState.Success(response))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    fun getListActivity(): Flow<DataState<List<ActivityDetailModel>>> = flow {
        emit(DataState.Loading)
        val url = String.format(buildUrl(ApiUrl.ApiName.ListActivity, null))
        try {
            val response: List<ActivityDetailModel> = apiServices.get(
                url
            )
            Log.d("getAthlete", response.toString())
            emit(DataState.Success(response))
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}