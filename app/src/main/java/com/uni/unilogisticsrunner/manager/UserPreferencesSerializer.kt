package com.uni.unilogisticsrunner.manager

import android.content.Context
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.dataStore
import com.google.protobuf.InvalidProtocolBufferException
import com.uni.unilogisticsrunner.UserLogin
import java.io.InputStream
import java.io.OutputStream


object SettingsSerializer : Serializer<UserLogin> {
    override val defaultValue: UserLogin = UserLogin.getDefaultInstance()

    override suspend fun readFrom(input: InputStream): UserLogin {
        try {
            return UserLogin.parseFrom(input)
        } catch (exception: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read proto.", exception)
        }
    }

    override suspend fun writeTo(
        t: UserLogin,
        output: OutputStream) = t.writeTo(output)
}

val Context.settingsDataStore: DataStore<UserLogin> by dataStore(
    fileName = "user_prefs.pb",
    serializer = SettingsSerializer
)