package com.uni.unilogisticsrunner.manager

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import java.io.IOException


class UserDataPreferences(val context: Context) {

    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
            name = "ACCESS_TOKEN",
            scope = CoroutineScope(Dispatchers.Default)
        )
        private val ACCESS_TOKEN = stringPreferencesKey("ACCESS_TOKEN")
        private val CURRENT_USER = stringPreferencesKey("CURRENT_USER")
        private val ID_USER = stringPreferencesKey("ID_USER")
        private val REFRESH_TOKEN = stringPreferencesKey("REFRESH_TOKEN")


    }

    fun saveToken(data: String?) {
        data?.let {
            runBlocking {
                context.dataStore.edit { preferences ->
                    preferences[ACCESS_TOKEN] = data
                }
            }
        }
    }

    fun saveRefreshToken(data: String?) {
        data?.let {
            runBlocking {
                context.dataStore.edit { preferences ->
                    preferences[REFRESH_TOKEN] = data
                }
            }
        }
    }

    private val refTokenData = context.dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
        .map { preferences ->
            preferences[REFRESH_TOKEN] ?: ""
        }

    fun getRefreshToken(): String? {
        return runBlocking {
            refTokenData.first()
        }
    }


    private val tokenData = context.dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
        .map { preferences ->
            preferences[ACCESS_TOKEN] ?: ""
        }

    fun getToken(): String? {
        return runBlocking {
            tokenData.first()
        }
    }

    fun saveAuthUser(data: String?) {
        data?.let {
            runBlocking {
                context.dataStore.edit { preferences ->
                    preferences[CURRENT_USER] = data
                }
            }
        }
    }

    private val userAuthData = context.dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
        .map { preferences ->
            preferences[CURRENT_USER] ?: ""
        }

    fun getAuthUser(): String? {
        return runBlocking {
            userAuthData.first()
        }
    }


    fun saveIdUser(data: String?) {
        data?.let {
            runBlocking {
                context.dataStore.edit { preferences ->
                    preferences[ID_USER] = data
                }
            }
        }
    }

    private val idUser = context.dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }
        .map { preferences ->
            preferences[ID_USER] ?: ""
        }

    fun getIdUser(): String? {
        return runBlocking {
            idUser.first()
        }
    }

    fun logout() {
        runBlocking {
            context.dataStore.edit { preferences ->
                preferences.clear()
            }
        }
    }

}


